
#include <deque>
#include <fstream>
#include <iostream>
#include <memory>

using namespace std;
namespace
{
struct XyzRgb
{
    float x=0.f, y=0.f, z=0.f;
    unsigned char r=0, g=0, b=0;
};
struct Xyz
{
    double x=0.f, y=0.f, z=0.f;
    inline float Norm2(){return x*x+y*y+z*z;}
};
Xyz operator -(const Xyz & P1, const Xyz & P2)
{
    Xyz P;
    P.x = P1.x-P2.x;
    P.y = P1.y-P2.y;
    P.z = P1.z-P2.z;
    return P;
}

/*struct PointD
    {
      double x, y, z;
        unsigned char r, g, b;
    };*/

void convert_asc_to_ply(std::istream& input, std::ostream& output, Xyz O,
                        double r_max, unsigned sub, Xyz Err)
{
    Xyz Pivot;
    Pivot.x=1051200.;
    Pivot.y=6841900.;
    double r_max2 = r_max*r_max;
    std::deque<XyzRgb> points;
    int i = 0;
    while (input.good())
    {
        //double x, y, z, w;
        int r,g,b;
        Xyz P;
        input >> P.x >> P.y >> P.z >> r >> g >> b;
        if(i%10000 == 0) cout << "." << flush;
        if(i++%sub == 0)
        {
            //cout << P.x << " " << P.y << " " << P.z << "+" << endl;
            if((P-O).Norm2() < r_max2)
            {
                Xyz dP = (P-Pivot)-Err;
                //cout << dP.x << " " << dP.y << " " << dP.z << endl;
                XyzRgb PC;
                PC.x = dP.x;
                PC.y = dP.y;
                PC.z = dP.z;
                PC.r = r;
                PC.g = g;
                PC.b = b;
                points.push_back(PC);
            }
        }
    }
    cout << endl << i << " points in input reduced to " << points.size() << endl;

    output << "ply" << std::endl;
    output << "format binary_little_endian 1.0" << std::endl;
    output << "comment pivot " << Pivot.x << " " << Pivot.y << " " << Pivot.z << std::endl;
    output << "element vertex " << points.size() << std::endl;
    output << "property float x" << std::endl;
    output << "property float y" << std::endl;
    output << "property float z" << std::endl;
    output << "property uchar red" << std::endl;
    output << "property uchar green" << std::endl;
    output << "property uchar blue" << std::endl;
    output << "end_header" << std::endl;

    for (auto p : points)
    {
        output.write(reinterpret_cast<const char*>(&p.x), sizeof(float) * 3);
        output.write(reinterpret_cast<const char*>(&p.r), sizeof(unsigned char) * 3);
    }
}
}

int main(int argc, char* argv[])
{
    cout << "Usage: " << argv[0] << " input_file.asc output_file.ply Ox Oy Oz r_max sub" << endl;
    if(argc<3) return 1;

    unique_ptr<istream> ifs;
    unique_ptr<ostream> ofs;
    int i = 1;
    if (argc > i)
        ifs = make_unique<ifstream>(argv[i++]);

    if (argc > i)
        ofs = make_unique<ofstream>(argv[i++]);

    Xyz O;
    if (argc > i) O.x = atof(argv[i++]);
    if (argc > i) O.y = atof(argv[i++]);
    if (argc > i) O.z = atof(argv[i++]);

    double r_max=100.;
    if (argc > i) r_max = atof(argv[i++]);

    unsigned sub=100;
    if (argc > i) sub = atof(argv[i++]);

    Xyz Err;
    Err.x=0.;
    Err.x=0.;
    Err.x=0.;
    if (argc > i) Err.x = atof(argv[i++]);
    if (argc > i) Err.y = atof(argv[i++]);
    if (argc > i) Err.z = atof(argv[i++]);

    convert_asc_to_ply(ifs ? *ifs : cin, ofs ? *ofs : cout, O, r_max, sub, Err);

    return 0;
}
