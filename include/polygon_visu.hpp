#include"Polygon.hpp"

///////////// simple 2d polygon creation

Polygon simple_Polygon_creation(  pcl::ModelCoefficients::Ptr pl, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{   

    Polygon out ;
    Lg::Point3f n(pl->values[0], pl->values[1], pl->values[2]);

    //n.Normalize();
    Lg::Point3f m = best_m(n);
    m.Normalize();
    Lg::Point3f k = n^m;
    k.Normalize();
    Lg::Point3f O=(-pl->values[3]/(n*n))*n;


    vector<Lg::Point2f> points_2d(cloud->points.size());
    std::vector<K::Point_2> points_cgal(points_2d.size());
    // TODO CALCULER O
    //Lg::Point3f O=(-pl->values[3]/((pl->values[0]*pl->values[0])+(pl->values[1]*pl->values[1])+(pl->values[2]*pl->values[2])))*n;



    for(int i = 0; i < cloud->points.size(); ++i)
    {//pcl::PointXYZ pt1=project(cloud->points[i],  pl);
        Lg::Point3f P=Lg::Point3f(cloud->points[i].x,cloud->points[i].y,cloud->points[i].z);

        Lg::Point3f  P0=P-O;
        float xp=P0*m, yp=P0*k;
        out.push_back(K::Point_2(xp, yp));}


    return out;

}

///////////// grid points generation

pcl::PointCloud<pcl::PointXYZ>::Ptr Grid_cloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,pcl::ModelCoefficients::Ptr pl)
{pcl::PointXYZ min_pt, max_pt;
    pcl::getMinMax3D(*cloud, min_pt, max_pt);




    std::vector<pcl::PointXYZ>VEC;
    float seed =0.02f;
    float k=(max_pt.z-min_pt.z)/2;

    for(float x=min_pt.x-0.1; x<max_pt.x+0.1; x+=0.01f)
    {
        for(float y=min_pt.y-0.1; y<max_pt.y+0.1; y+=0.01f)//
        {float z;
            if(pl->values[2]==0)
                z=k;
            else
                z=-(pl->values[3]+pl->values[0]*x+pl->values[1]*y)/pl->values[2];
            pcl::PointXYZ pt=pcl::PointXYZ(x,y,z);

            VEC.push_back(pt);
        }}
    pcl::PointCloud<pcl::PointXYZ>::Ptr output(new pcl::PointCloud<pcl::PointXYZ>);

    output->width    = VEC.size();
    output->height   = 1;
    output->is_dense = false;

    for(int i=0;i<VEC.size(); i++)
    {


        output->points.push_back(VEC[i]);
    }
    return output;
}


//////////////  grid polygon creation for simplifing the visualization

pcl::PointCloud<pcl::PointXYZ> Grid_polygon(  pcl::PointCloud<pcl::PointXYZ>::Ptr coll,pcl::ModelCoefficients::Ptr pl)
{    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);


     std::vector<pcl::PointCloud<pcl::PointXYZ>> out_data;
      pcl::PointCloud<pcl::PointXYZ>polygon_hul ;
       pcl::PointCloud<pcl::PointXYZ>::Ptr Cloud(new pcl::PointCloud<pcl::PointXYZ>) ;
        pcl::PointCloud<pcl::PointXYZ> Out;

         Polygon p,p1;
          Cloud=Grid_cloud(coll,pl);

           // pcl::io::savePLYFile("Grid.ply",Cloud);
           p=simple_Polygon_creation(pl,coll);
            p1=simple_Polygon_creation(pl,Cloud);
             std::vector<int>indices;

              //CGAL::draw(p1);
              for (int i = 0; i < p1.size(); ++i)
              {
                  bool IsInside = (p.bounded_side(p1[i]) == CGAL::ON_BOUNDED_SIDE);
                  bool IsInside2 = (p.bounded_side(p1[i]) == CGAL::ON_BOUNDARY);
                  if((IsInside==true)||(IsInside2==true))
                      indices.push_back(i);
              }

               Out.width    = indices.size();
                Out.height   = 1;
                 Out.is_dense = false;



                  for(int m=0; m<indices.size(); m++)
                  {Out.points.push_back(Cloud->points[indices[m]]);


                  }




                   return Out;
}
