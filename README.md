planar polygons based registration framework by Rahima Djahel(ENPC), Bruno Vallet(IGN) and  Pascal Monasse(ENPC)

Required dependencies: PCL, Eigen, CGAL,Boost

Three executables:

asc2ply to convert asc files to ply files

PolyDetect to extract planar polygons form LiDAR data

Poly_registration: to register  two LiDAR scans using Planar polygons 


Test:


./asc2ply   input_file.asc output_file.ply Ox Oy Oz r_max sub

 where
 
 (Ox,Oy,Oz): laser center coordinates
 
 r_max: the maximum radius 
 
 sub:resampling parameter 


 //////////////////////////////////////////////////////
 
 ./PolyDetect../data/indoor_scan 0.04 1000 1000 0.05
 
where:

0.04: inliers threshold 
1000: maximum number of the iteration 
1000: minimum size of a planar region 
0.05: the parameter alpha (alpha shape)
(the values of these parameters can be modified and adapted by the users )

/////////////////////////////////////////////////////////////////////////////////////

./Poly_registration ../data/indoor_scan.ply ../data/STR_Int.ply 0.04 1000 700 0.05 30

where:

indoor_scan: the indoor scan
STR_Int: indoor points detected from exterior scans 
0.04: inliers threshold 
1000: maximum number of the iteration 
1000: minimum size of a planar region 
0.05: the parameter alpha (alpha shape)
30: distance threshold
(the values of these parameters can be modified and adapted by the users )


to visualize the result:

cloudcompare.CloudCompare  TR_indoor_scan.ply  ../data/outdoor_scan.ply

Remark :for poly_registration

As the size of the 3D point cloud representing the indoor points seen from the outdoor is very small compared to the size of the indoor scan, It is preferred not to use the same threshold of inliers (to detect planar polygons) as well as the minimum size of a planar region.

that's why we have fixed these two parameters in Poly_registration.cpp in order to be compatible with our data size 



If you use our algorithm in any of your publications or projects, please cite our paper:

Djahel, Rahima, Bruno Vallet, and Pascal Monasse. "Towards Efficient Indoor/outdoor Registration Using Planar Polygons." ISPRS Annals of the Photogrammetry, Remote Sensing and Spatial Information Sciences 2 (2021): 51-58.


If you have any questions, you can send an email to :

rahima.djahel@enpc.fr

rdjahel@gmail.com
